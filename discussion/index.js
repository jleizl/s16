// console.log("hello!");

// Arithmetic Operators
/*
	+ sum
	- subtraction
	* multiplication
	/ division
	% modulo (returns the remainder)
*/

let x = 45;
let y = 28;


let sum = x + y;
console.log("Result of additon: " + sum);

let difference = x - y;
console.log("Result of subtraction: " + difference);

let product = x * y;
console.log("Result of multiplication: " + product);

let quotient = x / y;
console.log("Result of division: " + quotient);

let mod = x % y;
console.log("Result of Modulo: " + mod);

// Assignment Operator
// Basic Assignment operator (=)
let assignmentNumber = 8;

// Arithmetic Assignment Operator
// Addition Assignment Operator (+=)
// Subtraction Assignmetn Operator (-=)
// Multiplication Assignment Operator (*=)
// Division Assignmetn Operator (/=)
// Modulo Assignment Operator (%=)

assignmentNumber = assignmentNumber + 2;
console.log("Result of addition operator: " + assignmentNumber);
assignmentNumber += 2;
console.log("")

/*
	P - Parenthesis
	E - Exponent
	M - Multiplication
	D - Division
	A - Addition
	S - Substraction
*/

let mdas = 1 + 2 -3 * 4 / 5;
console.log("Result of mdas: " + mdas);

let pemdas = 1 + (2 - 3) * (4 / 5);
console.log("Result of pemdas: " + pemdas);


// Increment and Decrement
	// Operators that add or subtract values by 1 and reassigns the value by 1 and reassigns the value of the variable where the increment/ decrement was applied for
	let z = 1;
	console.log(z);

	let increment = ++z;
	console.log("Pre-increment: " + increment);
	console.log("Value of z: " + z);

	increment = z++;
	console.log("Post-increment: " + increment);
	console.log("Value of z: " + z);

	let decrement = --z
	console.log("pre-decrement: " + decrement);
	console.log("Value of z: " + z);

	decrement = z--
	console.log("Post-decrement: " + decrement);

	console.log("Value of z: " + z);

// Type Coercion
	let numA = '10';
	let numB = 12;

	let coercion = numA + numB;

	console.log(coercion);
	console.log(typeof coercion);
	
	let numC = 16;
	let numD = 14;
	let nonCoercion = numC + numD;
	console.log(nonCoercion);

	let numE = true + 1; 
	console.log(numE);

	let numF = false + 2;
	console.log(numF);


// Equality Operator
	console.log(1 == 1);
	console.log(1 == 2);
	console.log(1 == '1');
	console.log('jungkook' == 'jungkook');
	console.log('Jungkook' == 'jungkook');
	console.log('a' == 'A');


// Strict Equality (===)
	console.log(1 === 1);
	console.log(1 === '1');

	let johnny = 'johnny'
	console.log('johnny' === johnny);
	console.log(false === 0);


// Inequality Operator
	console.log(1 != 1);
	console.log(1 != 3);
	console.log(1 != '1');
	console.log('jungkook' != 'jungkook');
	console.log('Jungkook' != 'jungkook');


// Strict Inequality Operator !==
	console.log(1 !== 1);
	console.log(1 !== '1');


// Relational Operator 
	let a = 50;
	let b = 65;

	let isGreaterThan = a > b;
	console.log(isGreaterThan);

	let isLessThan = a < b;
	console.log(isLessThan);

	let GTE = a >= b;
	console.log(GTE);

	let LTE = a <= b;
	console.log(LTE);

	let numStr = '30';
	console.log(a > numStr);

	let str = 'twenty'
	console.log(b >= str);


// Logical Operator (&&, ||)
	
	// && AND

	let isAdmin = false;
	let isRegistered = true;
	let isLegalAge = true;

	let authorization1 = isAdmin && isRegistered;
	console.log(authorization1);
	let voteAuthorization = isLegalAge && isRegistered;
	console.log(voteAuthorization);

	let adminAccess = isAdmin && isLegalAge && isRegistered;
	console.log(adminAccess);

	let random = isAdmin && false;
	console.log(random);

	let requiredLevel = 95;
	let requiredAge = 18; 

	let gameTopPlayer = isRegistered && requiredLevel === 25;
	console.log(gameTopPlayer); 

	let gamePlayer = isRegistered && isLegalAge && requiredLevel >= 25;
	// console.log(requiredLevel >= 25);
	console.log(gamePlayer);

	let userName = 'kookie2022';
	let userName2 = 'gamer01';
	let userAge = 15; 
	let userAge2 = 26; 

	let registration1 = userName.length > 8 && userAge >= requiredAge;
	console.log(registration1);

	let registration2 = userName2.length > 8 && userAge2 >= requiredAge;
	console.log(registration2);

	// || or
	let userlevel = 100;
	let userlevel2 = 65;

	let guildRequirement = isRegistered || userlevel >= requiredLevel || userAge >= requiredAge;
	console.log(guildRequirement);

	let guildAdmin = isAdmin || userlevel2 >= requiredLevel;
	console.log(guildAdmin);

	// Not operator ! - it returns the opposite value
	let guildAdmin2 = !isAdmin || userlevel2 >= requiredLevel;
	console.log(guildAdmin2);


	console.log(!isRegistered);

